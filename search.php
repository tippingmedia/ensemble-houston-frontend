<?php include 'inc/helpers.php'; ?>

<?php ob_start(); ?>

<?php echo ens_partial('header'); ?>

<?php echo ens_partial('spacer'); ?>

<div class="container max-w-600">
	<div class="text-center">
		<h1 class="h-strong-36">“longer search term here”</h1>
		<p class="h-24-upper text-gray mt-10">213 results</p>
	</div>

	<?php for ($i = 0; $i < 10; $i++) : ?>
		<div class="spacer-md"></div>

		<a href="#" class="block hover:text-blue">
			<h2 class="h-24">Search result title goes here</h2>
			<p class="text-gray-light uppercase text-12 tracking-1 mt-5">ensemblehouston.com/url/to/result</p>
			<p class="mt-10 text-14">Nullam id dolor id nibh ultricies vehicula ut id elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
		</a>
	<?php endfor; ?>
</div>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('back-to-top'); ?>

<?php echo ens_partial('spacer-md'); ?>

<?php echo ens_partial('footer'); ?>

<?php echo ens_minify(ob_get_clean());
