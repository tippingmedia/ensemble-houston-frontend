<?php include 'inc/helpers.php'; ?>

<?php ob_start(); ?>

<?php echo ens_partial('header'); ?>

<div class="relative z-1">
	<div class="container py-25 lg:center-in-parent">
		<div class="row -mx-10">
			<div class="col lg:w-1/2 px-10">
				<?php echo ens_partial('search-form'); ?>
			</div>

			<div class="col lg:w-1/2 mt-25 lg:mt-0 px-10">
				<select class="big-select">
					<option>Browse all categories</option>
					<option>Lorem</option>
					<option>Ipsum</option>
					<option>Dolor</option>
				</select>
			</div>
		</div>
	</div>
</div>

<?php if (isset($_GET['search'])) : ?>

	<div class="section bg-gray-lightest md:rounded-b-15">
		<div class="container max-w-800">
			<div class="text-center mb-40">
				<p class="h-24 leading-150p text-gray styled-links">You searched for “<a href="#">something</a>” in “<a href="#">some category</a>”</p>
				<p class="uppercase font-extrabold tracking-1 mt-10">5 events found</p>
				<a href="events.php" class="cta-link text-blue mt-20">Show all Events <?php echo ens_icon('cta-link-arrow', 10); ?></a>
			</div>

			<?php for ($i = 0; $i < 5; $i++) : ?>
				<div class="mt-15" data-aos="fade-up">
					<?php echo ens_partial('event'); ?>
				</div>
			<?php endfor; ?>
		</div>
	</div>

<?php else : ?>

	<div class="section bg-gray-lightest md:rounded-b-15" data-tabs>
		<div class="container mb-40">
			<div class="tabs-tabs tabs-tabs-line-behind">
				<div class="tabs-tabs-inner">
					<button class="tabs-tab open" data-tab="featured">Featured</button>
					<button class="tabs-tab" data-tab="week">This Week</button>
					<button class="tabs-tab" data-tab="calendar">Calendar</button>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="tabs-content max-w-800 mx-auto open" data-tab-content="featured">
				<p class="mb-40 text-center text-gray styled-links">Looking for a <a href="">Seating Chart</a>, <a href="#">Season Tickets</a>, or <a href="#">Membership Packages</a>?</p>

				<div class="mt-15" data-aos="fade-up">
					<a href="#" class="event <?php echo $classes ?? ''; ?>">
						<div class="event-poster">
							<img src="temp/poster2.jpg" alt="">
						</div>

						<div class="event-body">
							<div class="event-body-bg" style="background-color: #9d6d2b"></div>
							<img src="temp/cast.jpg" class="event-body-bg-img" style="opacity: .15" alt="">

							<div class="event-info">
								<p class="event-label">Family Friendly</p>
								<h2 class="event-title">‘da Kink In My Hair</h2>
								<p class="event-date">February 21st – March 21st</p>
							</div>

							<div class="event-cta">
								<span class="event-btn btn btn-sm btn-outline-trans-white"><span class="btn-inner">Get Tickets</span></span>
								<div class="event-price">From $25</div>
							</div>
						</div>
					</a>
				</div>

				<?php for ($i = 0; $i < 5; $i++) : ?>
					<div class="mt-15" data-aos="fade-up">
						<?php echo ens_partial('event'); ?>
					</div>
				<?php endfor; ?>
			</div>

			<div class="tabs-content max-w-1000 mx-auto" data-tab-content="week">
				<img src="https://placehold.it/2000x2000?text=Week%20Embed" alt="">
			</div>

			<div class="tabs-content max-w-1000 mx-auto" data-tab-content="calendar">
				<img src="https://placehold.it/2000x2000?text=Calendar%20Embed" alt="">
			</div>
		</div>
	</div>

<?php endif; ?>

<div class="-mt-20 relative z-1">
	<?php echo ens_partial('back-to-top'); ?>
</div>

<?php echo ens_partial('spacer-md'); ?>

<?php echo ens_partial('footer'); ?>

<?php echo ens_minify(ob_get_clean());
