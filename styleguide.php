<?php include 'inc/helpers.php'; ?>

<?php ob_start(); ?>

<?php echo ens_partial('header'); ?>

<?php echo ens_partial('spacer-md'); ?>

<?php echo ens_partial('breadcrumb'); ?>

<?php echo ens_partial('spacer-sm'); ?>

<?php echo ens_partial('rich-text'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('quote'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('rich-text'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('feature-image'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('3-up-columns'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('media-features'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('rich-text'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('team'); ?>

<?php echo ens_partial('spacer-sm'); ?>

<?php echo ens_partial('feature-no-image'); ?>

<?php echo ens_partial('spacer-sm'); ?>

<?php echo ens_partial('3-up-feature'); ?>

<?php echo ens_partial('spacer-sm'); ?>

<?php echo ens_partial('simple-cta'); ?>

<?php echo ens_partial('spacer-sm'); ?>

<?php echo ens_partial('single-feature'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('icon-blocks'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('back-to-top'); ?>

<?php echo ens_partial('spacer-md'); ?>

<?php echo ens_partial('footer'); ?>

<?php echo ens_minify(ob_get_clean());
