<?php include 'inc/helpers.php'; ?>

<?php ob_start(); ?>

<?php echo ens_partial('header'); ?>

<div class="pt-25 pb-50 bg-gray-dark text-white md:rounded-b-15">
	<div class="container">
		<div class="row items-center justify-between">
			<div class="col md:w-1/2 lg:w-5/12">
				<h2 class="h-22">Welcome to The Ensemble Theatre</h3>
				<p class="italic mt-10 text-gray-light">Founded in 1976 by the late George Hawkins, our mission is to preserve African American artistic expression and enlighten, entertain and enrich a diverse community.</p>
				<a href="#" class="cta-link text-white mt-10">Read our story <?php echo ens_icon('cta-link-arrow', 10); ?></a>
				<?php echo ens_partial('social', ['classes' => 'mt-30', 'btn_classes' => 'shadow-sm hover:shadow-lg']); ?>
			</div>

			<div class="col md:w-1/2 mt-50 md:mt-0 aos-big-buttons">
				<div class="row -mx-10">
					<div class="col w-1/2 px-10" data-aos="fade-up" data-aos-anchor=".aos-big-buttons">
						<a href="#" class="btn btn-outline-trans btn-outline-trans-lg block">
							<span class="btn-inner p-20">
								<?php echo ens_icon('calendar', 48); ?>
								<span class="block mt-10 md:text-16">Event Calendar</span>
							</span>
						</a>
					</div>

					<div class="col w-1/2 px-10" data-aos="fade-up" data-aos-delay="200" data-aos-anchor=".aos-big-buttons">
						<a href="#" class="btn btn-outline-trans btn-outline-trans-lg block">
							<span class="btn-inner p-20">
								<?php echo ens_icon('ticket', 48); ?>
								<span class="block mt-10 md:text-16">Season Tickets</span>
							</span>
						</a>
					</div>

					<div class="col w-1/2 px-10 mt-20" data-aos="fade-up" data-aos-delay="300" data-aos-anchor=".aos-big-buttons">
						<a href="#" class="btn btn-outline-trans btn-outline-trans-lg block">
							<span class="btn-inner p-20">
								<?php echo ens_icon('location-pin', 48); ?>
								<span class="block mt-10 md:text-16">Visitor Info</span>
							</span>
						</a>
					</div>

					<div class="col w-1/2 px-10 mt-20" data-aos="fade-up" data-aos-delay="400" data-aos-anchor=".aos-big-buttons">
						<a href="#" class="btn btn-outline-trans btn-outline-trans-lg block">
							<span class="btn-inner p-20">
								<?php echo ens_icon('heart-circle', 48); ?>
								<span class="block mt-10 md:text-16">Support Us</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-sm bg-gray-lightest md:rounded-15 md:text-center mt-150">
	<div class="container -mt-200">
		<div class="row justify-center -mt-50">
			<?php for ($i = 0; $i < 3; $i++) : ?>
				<div class="col sm:w-1/2 lg:w-1/3 mt-50" data-aos="fade-up" data-aos-delay="<?php echo (($i % 3) * 100) + 100; ?>">
					<?php echo ens_partial('card'); ?>
				</div>
			<?php endfor; ?>
		</div>
	</div>
</div>

<?php echo ens_partial('spacer-sm'); ?>

<?php echo ens_partial('feature-image'); ?>

<?php echo ens_partial('spacer'); ?>

<div class="container md:text-center">
	<p class="h-24 leading-150p text-gray styled-links">Looking for a <a href="">Seating Chart</a>, <a href="#">Season Tickets</a>, or <a href="#">Membership Packages</a>?</p>
</div>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('footer'); ?>

<?php echo ens_minify(ob_get_clean());
