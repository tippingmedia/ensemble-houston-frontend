<?php include 'inc/helpers.php'; ?>

<?php ob_start(); ?>

<?php echo ens_partial('header'); ?>

<div class="section bg-gray-lightest md:rounded-b-15 md:pt-25 lg:pt-0">
	<div class="container md:flex md:pb-50">
		<div class="sm:flex md:block flex-no-shrink md:w-200 text-center">
			<a href="#" class="btn btn-alt btn-alt-pink px-0 w-100p sm:w-50p md:w-100p"><strong>Get Tickets</strong> from $25.00</a>
			<a href="#" class="btn btn-alt btn-alt-gray px-0 w-100p sm:w-50p md:w-100p sm:ml-10 md:ml-0 mt-10 sm:mt-0 md:mt-5"><strong>Group Tickets</strong> 20 seats or more</a>
			<p class="uppercase text-14 font-extrabold mt-10"><a href="#">Box Office: 713.520.0055</a></p>
		</div>

		<div class="mt-50 md:mt-0 md:ml-50">
			<h3 class="h-24-upper">Synopsis</h3>
			<p class="uppercase mt-20 mb-5">Written by Johnny Appleseed</p>
			<p class="text-gray italic" data-truncate="2">A group of friends come together following the loss of a family matriarch. They must choose one song to sing at her Memorial that night. These five friends find solace in their sisterly bond and take a musical <span data-truncate-expand>&hellip; <a href="#" class="roman text-pink underline hover:text-gray-dark">Read More</a></span> <span class="hidden" data-truncate-hidden>and historical journey morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</span></p>
		</div>
	</div>
</div>

<div class="container my-50 md:-mb-50 md:-mt-100">
	<div class="p-50 bg-gray-dark rounded-15 text-gray-light text-15 shadow relative z-1">
		<div class="max-w-1000 mx-auto">
			<div class="row lg:-mx-40">
				<div class="col lg:w-1/3 lg:px-40">
					<div data-aos="fade-up">
						<h3 class="mb-10 uppercase font-extrabold text-white tracking-1">Duration</h3>

						<ul class="list-reset">
							<li>2 hours 15 minutes</li>
							<li>1 Intermission</li>
						</ul>

						<ul class="list-reset">
							<li><a href="#" class="cta-link text-white">Get Tickets from $25.50 <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
							<li><a href="#" class="cta-link text-white">Group Tickets <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
							<li><a href="#" class="cta-link text-white">Membership Packages <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
							<li><a href="#" class="cta-link text-white">Season Tickets <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
						</ul>
					</div>
				</div>

				<div class="col lg:w-1/3 lg:px-40 pt-20 mt-20 lg:pt-0 lg:mt-0 border-t-4 lg:border-t-0 lg:border-l-4 border-white-10">
					<div data-aos="fade-up" data-aos-delay="200">
						<h3 class="mb-10 uppercase font-extrabold text-white tracking-1">Audience</h3>
						<p>Not intended for children under 4. The show is recommended for ages 8+. Everyone, regardless of age, must have a ticket.</p>
						<a href="#" class="cta-link text-white">Ask a Question <?php echo ens_icon('cta-link-arrow', 10); ?></a>
					</div>
				</div>

				<div class="col lg:w-1/3 lg:px-40 pt-20 mt-20 lg:pt-0 lg:mt-0 border-t-4 lg:border-t-0 lg:border-l-4 border-white-10">
					<div data-aos="fade-up" data-aos-delay="300">
						<h3 class="mb-10 uppercase font-extrabold text-white tracking-1">Location</h3>
						<p>Ensemble Theatre<br><em>Audrey Lawson Arena</em></p>

						<ul class="list-reset">
							<li><a href="#" class="cta-link text-white">Get Directions <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
							<li><a href="#" class="cta-link text-white">Parking Info <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
							<li><a href="#" class="cta-link text-white">Airport &amp; Transit <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
							<li><a href="#" class="cta-link text-white">Hotels Nearby <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section pb-50 bg-pink text-white-80 md:rounded-15">
	<div class="container max-w-1100">
		<div class="row items-center">
			<div class="col xl:w-3/4">
				<ul class="list-reset h-36">
					<li><strong class="text-white font-extrabold uppercase">Previews:</strong> November 10, 11, and 14</li>
					<li class="mt-5"><strong class="text-white font-extrabold uppercase">Opening Night:</strong> November 15, 2018</li>
					<li class="mt-5"><strong class="text-white font-extrabold uppercase">Runs:</strong> November 15 - December 30, 2018</li>
				</ul>
			</div>

			<div class="col xl:w-1/4 mt-50 xl:mt-0 text-center">
				<div class="btn-group justify-center">
					<a href="#" class="btn btn-alt btn-alt-pink xl:px-0 w-100p sm:w-auto xl:w-100p"><strong>Get Tickets</strong> from $25.00</a>
					<a href="#" class="btn btn-alt btn-alt-outline xl:px-0 text-white w-100p sm:w-auto xl:w-100p mt-5"><strong>Group Tickets</strong> 20 seats or more</a>
				</div>

				<p class="uppercase text-14 font-extrabold mt-10 text-white"><a href="#">Box Office: 713.520.0055</a></p>
			</div>
		</div>
	</div>
</div>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('rich-text'); ?>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('media-features'); ?>

<?php echo ens_partial('spacer'); ?>

<div class="section pt-0 bg-gray-lightest md:rounded-15 md:text-center" data-tabs>
	<div class="container tabs-tabs mb-50">
		<div class="tabs-tabs-inner -mt-25">
			<button class="tabs-tab open" data-tab="cast">Cast</button>
			<button class="tabs-tab" data-tab="creative">Creative</button>
		</div>
	</div>

	<div class="container max-w-1000">
		<div class="tabs-content open" data-tab-content="cast">
			<div class="row justify-center -mt-50">
				<?php for ($i = 0; $i < 8; $i++) : ?>
					<div class="col w-1/2 md:w-1/3 lg:w-1/4 mt-50" data-aos="fade-up" data-aos-delay="<?php echo (($i % 4) * 100) + 100; ?>">
						<?php echo ens_partial('card-person'); ?>
					</div>
				<?php endfor; ?>
			</div>
		</div>

		<div class="tabs-content" data-tab-content="creative">
			<div class="row justify-center -mt-50">
				<?php for ($i = 0; $i < 8; $i++) : ?>
					<div class="col w-1/2 md:w-1/3 lg:w-1/4 mt-50" data-aos="fade-up" data-aos-delay="<?php echo (($i % 4) * 100) + 100; ?>">
						<?php echo ens_partial('card-person'); ?>
					</div>
				<?php endfor; ?>
			</div>
		</div>
	</div>
</div>

<?php echo ens_partial('spacer'); ?>

<div class="container text-center">
	<div class="btn-group justify-center">
		<a href="#" class="btn btn-alt btn-alt-pink w-100p sm:w-auto"><strong>Get Tickets</strong> from $25.00</a>
		<a href="#" class="btn btn-alt btn-alt-gray w-100p sm:w-auto"><strong>Group Tickets</strong> 20 seats or more</a>
	</div>

	<p class="uppercase text-14 mt-20 font-extrabold"><a href="#">Box Office: 713.520.0055</a></p>
</div>

<?php echo ens_partial('spacer'); ?>

<?php echo ens_partial('supported-by'); ?>

<?php echo ens_partial('spacer-md'); ?>

<?php echo ens_partial('back-to-top'); ?>

<?php echo ens_partial('spacer-md'); ?>

<?php echo ens_partial('footer'); ?>

<?php echo ens_minify(ob_get_clean());
