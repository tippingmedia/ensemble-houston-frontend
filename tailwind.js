let colors = {
	'inherit': 'inherit',
	'transparent': 'transparent',
	'current': 'currentColor',
	'transparent': 'transparent',

	'white': '#fff',
	'white-10': 'rgba(255, 255, 255, .1)',
	'white-40': 'rgba(255, 255, 255, .4)',
	'white-80': 'rgba(255, 255, 255, .8)',
	'black': '#000',

	'pink': '#8d338a',
	'gray-lightest': '#f3f5f6',
	'gray-lighter': '#ccd6dc',
	'gray-light': '#a8b0b5',
	'gray': '#6d7275',
	'gray-dark': '#1b3341',
	'gray-darker': '#0b141a',
	'blue-light': '#8c86ff',
	'blue': '#5a52ff',
};

let wh = {
	'0': '0',
	'auto': 'auto',
	'200': '200px',
	'50p': '50%',
	'100p': '100%',
	'100vw': '100vw',
	'100vh': '100vh',
};

let mmwh = global.Object.assign({
	'none': 'none',
}, wh);

let spacing = {
	'0': '0',
	'5': '5px',
	'10': '10px',
	'15': '15px',
	'20': '20px',
	'25': '25px',
	'30': '30px',
	'40': '40px',
	'50': '50px',
	'60': '60px',
	'100': '100px',
	'150': '150px',
	'200': '200px',
};

let margin = global.Object.assign({
	'auto': 'auto',
}, spacing);

let columns = {
	'5/6': '83.33333%',
	'3/4': '75%',
	'2/3': '66.66667%',
	'7/12': '58.33333%',
	'1/2': '50%',
	'5/12': '41.66667%',
	'1/3': '33.33333%',
	'1/4': '25%',
	'1/5': '20%',
	'1/6': '16.66667%',
	'1/12': '8.33333%',
};

module.exports = {

	colors: colors,

	screens: {
		'sm': '576px',
		'md': '768px',
		'lg': '992px',
		'xl': '1200px',
	},

	fonts: {
		'sans': ['Poppins', 'sans-serif'],
		'mono': ['monospace'],
	},

	textSizes: {
		'11': '11px',
		'12': '12px',
		'13': '13px',
		'14': '14px',
		'15': '15px',
		'16': '16px',
		'18': '18px',
		'24': '24px',
		'36': '36px',
		'52': '52px',
		'60': '60px',
	},

	fontWeights: {
		'light': '300',
		'normal': '400',
		'medium': '500',
		'bold': '700',
		'extrabold': '800',
	},

	leading: {
		'20': '20px',
		'24': '24px',
		'100p': '1',
		'125p': '1.25',
		'150p': '1.5',
		'175p': '1.75',
		'200p': '2',
	},

	tracking: {
		'-1': '-1px',
		'1': '1px',
		'2': '2px',
		'3': '3px',
	},

	textColors: colors,

	backgroundColors: colors,

	backgroundSize: {
		'auto': 'auto',
		'cover': 'cover',
		'contain': 'contain',
	},

	borderWidths: {
		default: '2px',
		'0': '0',
		'4': '4px',
	},

	borderColors: global.Object.assign({
		default: colors['gray-lighter'],
	}, colors),

	borderRadius: {
		default: '8px',
		'inherit': 'inherit',
		'0': '0',
		'15': '15px',
		'full': '9999px',
	},

	width: global.Object.assign(columns, wh),

	height: wh,

	minWidth: mmwh,

	minHeight: mmwh,

	maxWidth: global.Object.assign({
		'600': '600px',
		'700': '700px',
		'800': '800px',
		'1000': '1000px',
		'1100': '1100px',
	}, mmwh),

	maxHeight: mmwh,

	padding: spacing,

	margin: global.Object.assign(margin, columns),

	negativeMargin: global.Object.assign(margin, columns),

	shadows: {
		default: '0 4px 25px rgba(0, 0, 0, .2)',
		'none': 'none',
		'sm': '0 3px 10px rgba(0, 0, 0, .2)',
		'lg': '0 20px 50px rgba(0, 0, 0, .4)',
	},

	zIndex: {
		'auto': 'auto',
		'0': '0',
		'1': '1',
		'2': '2',
		'10': '10',
		'20': '20',
		'30': '30',
		'40': '40',
		'50': '50',
	},

	opacity: {
		'0': '0',
		'10': '.1',
		'20': '.2',
		'30': '.3',
		'40': '.4',
		'50': '.5',
		'60': '.6',
		'70': '.7',
		'80': '.8',
		'90': '.9',
		'100': '1',
	},

	svgFill: colors,

	svgStroke: colors,

	modules: {
		appearance: [],
		backgroundAttachment: false,
		backgroundColors: ['hover'],
		backgroundPosition: [],
		backgroundRepeat: [],
		backgroundSize: [],
		borderCollapse: false,
		borderColors: ['hover'],
		borderRadius: ['responsive'],
		borderStyle: [],
		borderWidths: ['responsive'],
		cursor: [],
		display: ['responsive'],
		flexbox: ['responsive'],
		float: [],
		fonts: [],
		fontWeights: [],
		height: ['responsive'],
		leading: [],
		lists: [],
		margin: ['responsive'],
		maxHeight: ['responsive'],
		maxWidth: ['responsive'],
		minHeight: ['responsive'],
		minWidth: ['responsive'],
		negativeMargin: ['responsive'],
		objectFit: [],
		objectPosition: [],
		opacity: ['hover'],
		outline: ['focus'],
		overflow: ['responsive'],
		padding: ['responsive'],
		pointerEvents: [],
		position: ['responsive'],
		resize: [],
		shadows: ['responsive', 'hover'],
		svgFill: [],
		svgStroke: [],
		tableLayout: false,
		textAlign: ['responsive'],
		textColors: ['hover', 'group-hover'],
		textSizes: ['responsive'],
		textStyle: ['hover'],
		tracking: [],
		userSelect: [],
		verticalAlign: [],
		visibility: [],
		whitespace: [],
		width: ['responsive'],
		zIndex: []
	},

	plugins: [],

	options: {
		prefix: '',
		important: false,
		separator: ':',
	},

};
