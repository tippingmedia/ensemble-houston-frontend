.menu-top {
	@apply flex flex-wrap list-reset justify-center -mx-10;
}

.menu-top-item {
	@apply mx-10;
}

.menu-header {
	@apply flex items-center list-reset text-white-80;
}

.menu-header-item {
}

.menu-header-item-parent {
	&:not(:hover):not(.hover) .menu-header-submenu {
		@apply invisible opacity-0;
	}

	&:hover .menu-header-link,
	&.hover .menu-header-link {
		@apply bg-white text-gray-darker;
	}
}

.menu-header-link {
	@apply relative block py-10 px-20 z-20 uppercase tracking-2 rounded-t font-bold;

	&:hover {
		@apply text-white;
	}
}

.menu-header-submenu {
	@apply absolute list-reset bg-white text-15 text-gray-darker p-10 z-10 shadow-lg rounded rounded-tl-0 transition;
	width: 220px;
}

.menu-header-subitem {

}

.menu-header-sublink {
	@apply relative block px-10 py-5 font-medium leading-150p z-0;

	&::before {
		content: '';
		@apply absolute pin bg-blue-gradient transition rounded;
		z-index: -1;
	}

	&:not(:hover)::before {
		@apply opacity-0;
	}

	&:hover {
		@apply text-white font-bold;
	}
}

.menu-modal {
	@apply list-reset -mx-20 text-left border-b bg-white;

	@screen md {
		@apply -mx-40 text-24;
	}
}

.menu-modal-item {
	@apply border-t relative;

	&.open {
		.menu-modal-toggle {
			transform: rotate(90deg);
		}

		.menu-modal-submenu {
			@apply block;
		}
	}
}

.menu-modal-link {
	@apply block py-10 px-20;

	@screen md {
		@apply px-40;
	}
}

.menu-modal-toggle {
	@apply absolute pin-t pin-r p-10 transition;

	&:focus {
		@apply outline-none;
	}

	@screen md {
		@apply p-15;
	}
}

.menu-modal-submenu {
	@apply list-reset hidden text-12 uppercase tracking-2 ml-10;

	@screen md {
		@apply ml-30 text-14;
	}
}

.menu-modal-subitem {
	@apply border-t;
}

.menu-modal-sublink {
	@apply block p-10;
}
