$('.header-slider').each(function() {
	var $this = $(this);

	var $bgs = $this.closest('header').find('.header-slider-bgs');
	var $prev = $this.find('.header-slider-arrow.prev');
	var $next = $this.find('.header-slider-arrow.next');
	var $slides = $this.find('.header-slider-slides');

	var totalSlides = $slides.children().length;

	$slides.on('change.flickity', function(ev, i) {
		if (i == 0) {
			$prev.prop('disabled', true);
		} else {
			$prev.removeAttr('disabled');
		}

		if (i == (totalSlides - 1)) {
			$next.prop('disabled', true);
		} else {
			$next.removeAttr('disabled');
		}
	});

	$bgs.flickity({
		prevNextButtons: false,
		pageDots: false,
		fade: true,
		asNavFor: $slides[0],
	});

	$slides.flickity({
		prevNextButtons: false,
		pageDots: false,
		fade: true,
		draggable: false,
		autoPlay: true,
		imagesLoaded: true,
	});

	$prev.on('click', function() {
		$slides.flickity('previous');
	});

	$next.on('click', function() {
		$slides.flickity('next');
	});
});
