$('.team').each(function() {
	var $this = $(this);

	var $prev = $this.find('.team-arrow.prev');
	var $next = $this.find('.team-arrow.next');
	var $bigThumb = $this.find('.team-big-thumb');
	var $thumbs = $this.find('.team-thumbs');
	var $thumb = $this.find('.team-thumb');
	var $bios = $this.find('.team-bios');

	var totalBios = $bios.children().length;

	$bios.on('ready.flickity', function() {
		$thumb.eq(0).addClass('active');
	});

	$bios.on('change.flickity', function(ev, i) {
		var $newThumb = $thumb.eq(i);

		$bigThumb.attr('src', $newThumb.attr('src'));
		$thumb.removeClass('active');
		$newThumb.addClass('active');

		if (i == 0) {
			$prev.prop('disabled', true);
		} else {
			$prev.removeAttr('disabled');
		}

		if (i == (totalBios - 1)) {
			$next.prop('disabled', true);
		} else {
			$next.removeAttr('disabled');
		}
	});

	$bios.flickity({
		wrapAround: true,
		cellAlign: 'left',
		prevNextButtons: false,
		pageDots: false,
	});

	$thumb.on('click', function() {
		$bios.flickity('select', $(this).index());
	});

	$prev.on('click', function() {
		$bios.flickity('previous');
	});

	$next.on('click', function() {
		$bios.flickity('next');
	});
});
