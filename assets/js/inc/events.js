$('.event').hover(
	function() {
		var $self = $(this);

		setTimeout(function() {
			$self.addClass('enable-btn-hover');
		}, 200);
	},
	function() {
		var $self = $(this);

		setTimeout(function() {
			$self.removeClass('enable-btn-hover');
		}, 200);
	}
);
