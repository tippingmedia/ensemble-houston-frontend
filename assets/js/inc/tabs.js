$('[data-tabs]').each(function() {
	var $this = $(this);
	var $tab = $this.find('[data-tab]');
	var $content = $this.find('[data-tab-content]');

	$tab.on('click', function(ev) {
		ev.preventDefault();

		var $this = $(this);
		var target = $this.data('tab');

		$content.find('.aos-animate').removeClass('aos-animate');
		$tab.add($content).removeClass('open');
		$this.add($content.filter('[data-tab-content="' + target + '"]')).addClass('open');

		ensAOS();
	});
});
