$('.menu-modal-toggle').on('click', function(ev) {
	ev.preventDefault();
	$(this).parent().toggleClass('open');
});

$('.menu-header-item-parent > a').on('touchstart', function(ev) {
	var $li = $(this).parent();

	if ($li.hasClass('hover')) {
		return true;
	} else {
		$li.addClass('hover');
		$('.menu-header-item').not($li).removeClass('hover');
		ev.preventDefault();
		return false;
	}
});
