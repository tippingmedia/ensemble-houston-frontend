function ensAOS() {
	if (navigator.userAgent.indexOf('MSIE') != -1 || navigator.userAgent.indexOf('Trident') != -1) {
		$('[data-aos]').addClass('aos-init aos-animate');
	} else {
		AOS.init({
			disable: 'mobile',
			delay: 100,
		});
	}
}

ensAOS();
