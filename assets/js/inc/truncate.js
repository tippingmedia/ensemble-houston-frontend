$('[data-truncate]').each(function() {
	var $this = $(this);
	var $expand = $this.find('[data-truncate-expand]');
	var $hidden = $this.find('[data-truncate-hidden]');

	$expand.on('click', function() {
		$expand.remove();
		$hidden.removeClass('hidden');
	});
});
