$.modal.defaults.fadeDuration = 200;
$.modal.defaults.blockerClass = 'modal-bg';
$.modal.defaults.showSpinner = false;
$.modal.defaults.closeClass = 'modal-close';
$.modal.defaults.closeText = '✕';

$('.modal').on($.modal.OPEN, function() {
	$(this).find('input[type="search"]').focus();
});
