<form action="#" class="search-form">
	<input type="search" name="" class="search-input" placeholder="I’m searching for …" required>
	<button type="button" class="search-btn btn btn-outline"><div class="btn-inner">Search</div></button>
	<?php echo ens_icon('search', 24, 'input-icon search-icon'); ?>
</form>
