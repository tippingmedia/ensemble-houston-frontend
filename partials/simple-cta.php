<div class="section bg-gray-lightest md:rounded-15">
	<div class="container md:flex items-center">
		<h2 class="h-28 leading-150p">The Mission of The Ensemble Theatre is to preserve African American artistic expression and enlighten, entertain and enrich a diverse community.</h2>

		<div class="flex-no-shrink mt-30 md:mt-0 md:ml-100">
			<div class="btn-group">
				<div data-aos="fade-left">
					<a href="#" class="btn btn-blue"><span class="btn-inner">This is a Button</span></a>
				</div>
			</div>
		</div>
	</div>
</div>
