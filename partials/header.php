<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo ucfirst(str_replace('.php', '', ENS_SCRIPT_NAME)); ?></title>
	<link rel="stylesheet" href="<?php echo ens_asset('dist/ensemble.min.css'); ?>">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,700,800">
</head>

<body class="md:p-20 flex flex-col min-h-100vh overflow-x-hidden">
	<div class="py-10 text-14 <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'text-gray' : 'bg-gray-darker text-gray-light'; ?>">
		<div class="container">
			<ul class="menu-top lg:justify-end">
				<li class="menu-top-item"><a href="#" class="menu-top-link <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'hover:text-gray-darker' : 'hover:text-white'; ?>">Season Tickets</a></li>
				<li class="menu-top-item"><a href="#" class="menu-top-link <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'hover:text-gray-darker' : 'hover:text-white'; ?>">Subscribe</a></li>
				<li class="menu-top-item"><a href="#" class="menu-top-link <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'hover:text-gray-darker' : 'hover:text-white'; ?>">Venue Rental</a></li>
				<li class="menu-top-item uppercase font-extrabold"><a href="tel:7135200055" class="menu-top-link <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'hover:text-gray-darker' : 'text-white hover:text-blue-light'; ?>">Box Office: 713.520.0055</a></li>
			</ul>
		</div>
	</div>

	<header class="relative bg-gray-dark text-white <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'md:rounded-t overflow-hidden' : ''; ?>">
		<?php echo ens_partial('header-bg'); ?>

		<div class="relative">
			<div class="py-20 <?php echo ENS_SCRIPT_NAME == 'events.php' ? 'bg-gray-dark' : ''; ?>">
				<div class="container flex items-end lg:items-center justify-between">
					<a href="index.php" class="mr-auto">
						<img src="<?php echo ens_asset('img/logo.png'); ?>" width="155" class="locked" alt="">
					</a>

					<ul class="menu-header hidden xl:flex">
						<li class="menu-header-item"><a href="#" class="menu-header-link">Our Story</a></li>
						<li class="menu-header-item"><a href="#" class="menu-header-link">Visit</a></li>

						<li class="menu-header-item menu-header-item-parent">
							<a href="#" class="menu-header-link">Events</a>

							<ul class="menu-header-submenu">
								<li class="menu-header-subitem"><a href="#" class="menu-header-sublink">Our Story</a></li>
								<li class="menu-header-subitem"><a href="#" class="menu-header-sublink">Visit</a></li>
								<li class="menu-header-subitem"><a href="#" class="menu-header-sublink">Initiatives</a></li>
								<li class="menu-header-subitem"><a href="#" class="menu-header-sublink">Support Us</a></li>
							</ul>
						</li>

						<li class="menu-header-item"><a href="#" class="menu-header-link">Initiatives</a></li>
						<li class="menu-header-item"><a href="#" class="menu-header-link">Support Us</a></li>
					</ul>

					<a href="#modal-menu" class="inline-block xl:hidden btn btn-outline-trans" rel="modal:open"><span class="btn-inner px-10"><?php echo ens_icon('navigation-show-more'); ?></span></a>
					<a href="#modal-menu" class="hidden xl:block leading-100p ml-10" rel="modal:open"><?php echo ens_srt('Search') . ens_icon('search', 20); ?></a>

					<div class="hidden sm:block" data-aos="fade-left">
						<a href="#" class="btn btn-blue shadow-sm hover:shadow-lg ml-10 xl:ml-30"><span class="btn-inner"><?php echo ens_icon('ticket', 24, 'btn-icon'); ?>Get Tickets</span></a>
					</div>
				</div>
			</div>

			<?php echo ens_partial('header-content'); ?>
		</div>

		<?php echo ens_partial('header-bottom'); ?>
	</header>

	<main class="flex-grow">
