<?php if (ENS_SCRIPT_NAME == 'event.php') : ?>

	<div class="header-bottom">
		<img src="<?php echo ens_asset('img/header-bottom-1-pink.svg'); ?>" class="header-bottom-1" alt="">
		<img src="<?php echo ens_asset('img/header-bottom-2-gray-lightest.svg'); ?>" class="header-bottom-2" alt="">
		<?php echo ens_partial('header-bottom-share'); ?>
	</div>

<?php elseif (ENS_SCRIPT_NAME == 'events.php') : ?>

	<?php // nothing ?>

<?php elseif (ENS_SCRIPT_NAME == 'home.php') : ?>

	<div class="header-bottom header-bottom-simple">
		<img src="<?php echo ens_asset('img/header-bottom-1-gray-dark.svg'); ?>" class="header-bottom-1" alt="">
	</div>

<?php elseif (ENS_SCRIPT_NAME == 'search.php') : ?>

	<div class="header-bottom">
		<img src="<?php echo ens_asset('img/header-bottom-2-white.svg'); ?>" class="header-bottom-2" alt="">
	</div>

<?php else : ?>

	<div class="header-bottom">
		<img src="<?php echo ens_asset('img/header-bottom-1-blue-gradient.svg'); ?>" class="header-bottom-1" alt="">
		<img src="<?php echo ens_asset('img/header-bottom-2-white.svg'); ?>" class="header-bottom-2" alt="">
		<?php echo ens_partial('header-bottom-share'); ?>
	</div>

<?php endif;
