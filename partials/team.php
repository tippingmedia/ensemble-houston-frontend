<?php $team = [
	'Oscar Greene',
	'Karen Carlson',
	'Carole Armstrong',
	'Beverly Walker',
	'Clark Hansen',
	'Kyle Goodwin',
	'Gertrude Salazar',
	'Milton Pierce',
	'Harriet Patton',
	'Eileen Aguilar',
	'Janie Mullins',
]; ?>

<div class="team section bg-gray-lightest md:rounded-15 md:text-center">
	<div class="container max-w-1000">
		<h2 class="h-24-upper mb-50">Meet our Team</h2>

		<div class="row relative">
			<button type="button" class="team-arrow btn btn-white text-blue prev" disabled><span class="btn-inner"><?php echo ens_icon('chevron-left'); ?></span></button>
			<button type="button" class="team-arrow btn btn-white text-blue next"><span class="btn-inner"><?php echo ens_icon('chevron-right'); ?></span></button>

			<div class="col md:w-1/2 lg:pr-40" data-aos="fade-right">
				<?php // using ?name as example to indicate which thumbnail is active ?>
				<img src="temp/person.jpg?name=<?php echo urlencode($team[0]); ?>" class="team-big-thumb" alt="">
			</div>

			<div class="flex flex-col col md:w-1/2 lg:pl-40">
				<div class="team-bios">
					<?php foreach ($team as $name) : ?>
						<div class="team-bio text-15">
							<h3 class="h-24"><?php echo $name; ?></h3>
							<p class="text-gray">Job Title</p>
							<p class="mt-30">Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor.</p>
							<p class="mt-10"><a href="#" class="cta-link text-blue">Email <?php echo explode(' ', $name)[0]; ?> <?php echo ens_icon('cta-link-arrow', 10); ?></a></p>
						</div>
					<?php endforeach; ?>
				</div>

				<div class="team-thumbs">
					<?php $i = 0; foreach ($team as $name) : ?>
						<img src="temp/person.jpg?name=<?php echo urlencode($name); ?>" class="team-thumb" alt="">
					<?php $i++; endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
