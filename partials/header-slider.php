<div class="section">
	<div class="header-slider relative container max-w-1000 text-center md:text-left">
		<button type="button" class="header-slider-arrow btn btn-outline-trans-white prev" disabled><span class="btn-inner"><?php echo ens_icon('chevron-left'); ?></span></button>
		<button type="button" class="header-slider-arrow btn btn-outline-trans-white next"><span class="btn-inner"><?php echo ens_icon('chevron-right'); ?></span></button>

		<div class="header-slider-slides">
			<div class="header-slider-slide">
				<div class="threeD-img">
					<img src="temp/poster2.jpg" width="160" class="locked" alt="">
				</div>

				<div class="mt-20 md:mt-0 md:ml-40">
					<h2 class="h-strong-60 text-shadow-white">Sistas: The Musical</h1>
					<p class="h-strong-22 mt-15">February 22nd – March 22nd</p>

					<div class="mt-20">
						<div class="btn-group justify-center md:justify-start">
							<a href="#" class="btn btn-sm btn-white"><span class="btn-inner">Get Tickets</span></a>
							<a href="#" class="btn btn-sm btn-outline-trans-white"><span class="btn-inner">More Info</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="header-slider-slide">
				<div class="threeD-img">
					<img src="temp/poster2.jpg" width="160" class="locked" alt="">
				</div>

				<div class="mt-20 md:mt-0 md:ml-40">
					<h2 class="h-strong-60 text-shadow-white">‘da Kink In My Hair</h1>
					<p class="h-strong-22 mt-15">September 20th – October 14th</p>

					<div class="mt-20">
						<div class="btn-group justify-center md:justify-start">
							<a href="#" class="btn btn-sm btn-white"><span class="btn-inner">Get Tickets</span></a>
							<a href="#" class="btn btn-sm btn-outline-trans-white"><span class="btn-inner">More Info</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
