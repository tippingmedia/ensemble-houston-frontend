<div class="container">
	<div class="row -mx-10">
		<div class="col px-10" data-aos="fade-in" data-aos-delay="100">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/3uEx8QO1DCo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>

		<div class="col md:w-1/2 mt-20 px-10" data-aos="fade-in" data-aos-delay="100">
			<?php echo ens_partial('photo-feature'); ?>
		</div>

		<div class="col md:w-1/2 mt-20 px-10" data-aos="fade-in" data-aos-delay="200">
			<?php echo ens_partial('photo-feature'); ?>
		</div>

		<div class="col lg:w-1/3 mt-20 px-10" data-aos="fade-in" data-aos-delay="100">
			<?php echo ens_partial('photo-feature'); ?>
		</div>

		<div class="col lg:w-1/3 mt-20 px-10" data-aos="fade-in" data-aos-delay="200">
			<?php echo ens_partial('photo-feature'); ?>
		</div>

		<div class="col lg:w-1/3 mt-20 px-10" data-aos="fade-in" data-aos-delay="300">
			<?php echo ens_partial('photo-feature'); ?>
		</div>
	</div>
</div>
