<div class="container text-center">
	<a href="#" class="inline-block align-top">
		<span class="inline-flex leading-100p p-10 rounded-full bg-blue-gradient text-white">
			<?php echo ens_icon('arrow-up'); ?>
		</span>

		<div data-aos="fade-down">
			<span class="block uppercase text-blue text-13 tracking-1 mt-10">Back to Top</span>
		</div>
	</a>
</div>
