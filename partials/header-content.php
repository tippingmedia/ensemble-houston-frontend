<?php if (ENS_SCRIPT_NAME == 'event.php') : ?>

	<div class="section md:pb-50 lg:pb-0 relative z-1 text-center md:text-left">
		<div class="container md:flex items-end md:items-center lg:-mb-50">
			<div class="threeD-img">
				<img src="temp/poster2.jpg" class="locked w-200" alt="">
			</div>

			<div class="mt-20 md:mt-0 md:ml-40">
				<h1 class="h-strong-60 text-shadow-pink">Sistas: The Musical</h1>
				<p class="h-strong-22">February 22nd, 2018 – March 22nd, 2018</p>
			</div>
		</div>
	</div>

<?php elseif (ENS_SCRIPT_NAME == 'events.php') : ?>

	<?php echo ens_partial('header-slider'); ?>

<?php elseif (ENS_SCRIPT_NAME == 'home.php') : ?>

	<?php echo ens_partial('header-slider'); ?>

<?php elseif (ENS_SCRIPT_NAME == 'styleguide.php') : ?>

	<div class="section md:my-50">
		<div class="container max-w-1000">
			<div class="md:w-3/4">
				<h1 class="h-strong-88 text-shadow-white">Our Story</h1>
				<p class="h-22 leading-150p mt-20">The Ensemble Theatre was founded in 1976 by the late George Hawkins to preserve African American artistic expression and enlighten, entertain and enrich a diverse community.</p>
			</div>
		</div>
	</div>

<?php endif;
