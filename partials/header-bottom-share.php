<div class="header-bottom-share">
	<div class="flex justify-center items-center">
		<span class="text-12 uppercase tracking-2">Share</span>
		<a href="#" class="ml-10 hover:scale-125p"><?php echo ens_icon('circle-email'); ?></a>
		<a href="#" class="ml-10 hover:scale-125p"><?php echo ens_icon('circle-facebook'); ?></a>
		<a href="#" class="ml-10 hover:scale-125p"><?php echo ens_icon('circle-twitter'); ?></a>
	</div>
</div>
