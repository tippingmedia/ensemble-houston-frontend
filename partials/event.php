<a href="#" class="event <?php echo $classes ?? ''; ?>">
	<div class="event-poster">
		<img src="temp/poster2.jpg" alt="">
	</div>

	<div class="event-body">
		<div class="event-body-bg" style="background-color: #8d338a"></div>
		<img src="temp/cast2.jpg" class="event-body-bg-img" style="opacity: .15" alt="">

		<div class="event-info">
			<p class="event-label">Family Friendly</p>
			<h2 class="event-title">Sistas: The Musical</h2>
			<p class="event-date">February 22nd – March 22nd</p>
		</div>

		<div class="event-cta">
			<span class="event-btn btn btn-sm btn-outline-trans-white"><span class="btn-inner">Get Tickets</span></span>
			<div class="event-price">From $25</div>
		</div>
	</div>
</a>
