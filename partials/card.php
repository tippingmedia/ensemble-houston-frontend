<a href="#" class="card">
	<div class="card-media">
		<img src="https://placehold.it/720" class="bg-img" alt="">
	</div>

	<div class="card-body">
		<h3 class="h-24">Card Title Here</h3>
		<p class="mt-10 text-15 text-gray">Curabitur blandit tempus porttitor. Cum sociis natoque penatibus et magnis dis parturient.</p>
	</div>
</a>
