<?php if (ENS_SCRIPT_NAME == 'event.php') : ?>

	<img src="temp/cast2.jpg" class="bg-img" alt="">
	<div class="layer bg-darken-img"></div>

<?php elseif (in_array(ENS_SCRIPT_NAME, ['events.php', 'home.php'])) : ?>

	<div class="header-slider-bgs">
		<div class="header-slider-bg" style="background-color: #8d338a">
			<img src="temp/cast.jpg" class="bg-img" style="opacity: .15" alt="">
		</div>

		<div class="header-slider-bg" style="background-color: #9d6d2b">
			<img src="temp/cast2.jpg" class="bg-img" style="opacity: .15" alt="">
		</div>
	</div>

<?php elseif (ENS_SCRIPT_NAME == 'search.php') : ?>

	<div class="layer bg-pink"></div>
	<img src="temp/cast2.jpg" class="bg-img" style="opacity: .15" alt="">

<?php elseif (ENS_SCRIPT_NAME == 'styleguide.php') : ?>

	<img src="temp/cast.jpg" class="bg-img" alt="">
	<div class="layer bg-darken-img"></div>

<?php endif;
