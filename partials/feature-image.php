<div class="section overflow-hidden bg-gray-dark md:rounded-15 text-white md:text-center">
	<img src="temp/cast.jpg" class="bg-img" style="opacity: .25" alt="">

	<div class="container relative">
		<div class="md:w-5/6 mx-auto">
			<h2 class="h-24-upper">Feature Title</h2>
			<p class="h-36 leading-150p mt-50">The Mission of The Ensemble Theatre is to preserve African American artistic expression and enlighten, entertain and enrich a diverse community.</p>

			<div class="mt-50">
				<div class="btn-group md:justify-center">
					<?php for ($i = 1; $i < 4; $i++) : ?>
						<div data-aos="fade-up" data-aos-delay="<?php echo $i * 100; ?>">
							<a href="#" class="btn btn-outline-trans-white"><span class="btn-inner">This is a Button</span></a>
						</div>
					<?php endfor; ?>
				</div>
			</div>
		</div>
	</div>
</div>
