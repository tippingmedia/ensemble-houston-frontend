<div class="section overflow-hidden md:rounded-15 text-white" style="background-color: #9d6d2b">
	<img src="temp/cast2.jpg" class="bg-img" style="opacity: .15" alt="">

	<div class="container relative">
		<div class="row items-center">
			<div class="col lg:w-1/2" data-aos="fade-right">
				<img src="temp/poster.jpg" class="locked shadow-lg rounded" alt="">
			</div>

			<div class="col md:w-3/4 lg:w-5/12 mx-auto mt-50 lg:mt-0">
				<h2 class="h-strong-28">‘da Kink In My Hair</h2>
				<p>September 20th – October 14th</p>
				<p class="mt-30">Set in a Brooklyn hair salon, ‘da Kink in My Hair gives voice to Black women who tell their unforgettable stories in a kaleidoscope of drumming, singing and dance. It is a testament to the challenges and triumphs in the lives of contemporary Black women, many of whom are immigrants to North America from the Caribbean. Mixing laughter and tears, revelation and inspiration, the intense stories of each woman are woven together in this powerful piece.</p>

				<div class="mt-30">
					<div class="btn-group">
						<a href="#" class="btn btn-sm btn-white"><span class="btn-inner">Get Tickets</span></a>
						<a href="#" class="btn btn-sm btn-outline-trans-white"><span class="btn-inner">More Info</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
