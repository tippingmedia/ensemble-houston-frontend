<div class="container">
	<div class="row -mt-50">
		<div class="col md:w-1/2 mt-50" data-aos="fade-up">
			<div class="icon-block bg-gray-dark text-white">
				<img src="temp/cast.jpg" class="bg-img rounded-15" style="opacity: .25" alt="">

				<span class="icon-block-icon">
					<?php echo ens_icon('bed-double', 48); ?>
				</span>

				<div class="relative">
					<h2 class="h-strong-24-upper">Where to Stay</h2>
					<p class="mt-30">Curabitur blandit tempus porttitor. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>

					<ul class="list-reset mt-20">
						<li><a href="#" class="cta-link">Directions &amp; Parking <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
						<li><a href="#" class="cta-link">Airport &amp; Transit <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
						<li><a href="#" class="cta-link">Hotels Nearby <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col md:w-1/2 mt-50" data-aos="fade-up" data-aos-delay="200">
			<div class="icon-block">
				<img src="temp/cast2.jpg" class="bg-img rounded-15" style="opacity: .15" alt="">

				<span class="icon-block-icon">
					<?php echo ens_icon('fork-spoon', 48); ?>
				</span>

				<div class="relative">
					<h2 class="h-strong-24-upper">Where to Eat</h2>
					<p class="mt-30">Curabitur blandit tempus porttitor. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>

					<ul class="list-reset mt-20">
						<li><a href="#" class="cta-link">Directions &amp; Parking <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
						<li><a href="#" class="cta-link">Airport &amp; Transit <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
						<li><a href="#" class="cta-link">Hotels Nearby <?php echo ens_icon('cta-link-arrow', 10); ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
