	</main>

	<div class="footer-cta">
		<div class="footer-top"></div>

		<div class="container py-50 md:flex items-center">
			<h2 class="h-24 leading-150p">Preserving African American artistic expression and enlightening, entertaining and enriching a diverse community.</h2>

			<div class="flex-no-shrink mt-30 md:mt-0 md:ml-100">
				<div class="btn-group">
					<div data-aos="fade-left">
						<a href="#" class="btn btn-white text-blue hover:text-gray-dark"><span class="btn-inner transition-none">Support our Work</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="bg-gray-dark text-gray-light md:rounded-b-15">
		<div class="section container">
			<div class="row justify-around">
				<div class="col sm:w-1/2 md:w-1/3 lg:w-1/4">
					<img src="<?php echo ens_asset('img/logo.png'); ?>" class="locked lg:-mt-20" width="150" alt="">
					<p class="leading-175p mt-20">3535 Main Street<br>Houston, TX 77002<br><span class="text-white">Box Office: <a href="tel:713-520-0055">713-520-0055</a></span></p>
				</div>

				<div class="col sm:w-1/2 md:w-1/3 lg:w-1/4 mt-50 sm:mt-0">
					<h2 class="text-12 uppercase font-normal tracking-2">Quicklinks</h2>

					<ul class="list-reset mt-20 text-14 tracking-1 leading-200p uppercase font-medium">
						<li><a href="#" class="hover:text-white">Get Tickets</a></li>
						<li><a href="#" class="hover:text-white">Directions</a></li>
						<li><a href="#" class="hover:text-white">Parking</a></li>
						<li><a href="#" class="hover:text-white">Employment</a></li>
					</ul>
				</div>

				<div class="col md:w-1/3 lg:w-1/4 mt-50 md:mt-0">
					<h2 class="text-12 uppercase font-normal tracking-2">Connect</h2>
					<?php echo ens_partial('social', ['classes' => 'mt-30', 'btn_classes' => 'shadow-sm hover:shadow-lg']); ?>
				</div>
			</div>
		</div>

		<hr class="bg-white opacity-10">

		<div class="py-30 text-14">
			<div class="container md:flex justify-between">
				<p>&copy; <?php echo date('Y'); ?> The Ensemble Theatre, All Rights Reserved. <a href="#" class="text-white font-medium hover:text-blue-light">Privacy</a></p>
				<p class="mt-10 md:mt-0"><a href="https://tippingmedia.com/" target="_blank">website by <span class="text-white">Tipping Media</span><img src="assets/img/tipping-media.svg" class="inline align-top ml-5" alt="Tipping Media"></a></p>
			</div>
		</div>
	</footer>

	<?php echo ens_partial('modal-menu'); ?>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="<?php echo ens_asset('dist/ensemble.js'); ?>"></script>
</body>
</html>
