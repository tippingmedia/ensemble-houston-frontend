<div class="photo-feature">
	<img src="temp/cast.jpg" class="bg-img" style="opacity: .25" alt="">

	<div class="photo-feature-inner">
		<div class="photo-feature-content">
			<h2 class="photo-feature-title h-24">Feature Photo Title</h2>
			<p class="mt-20 text-white-80">Curabitur blandit tempus porttitor. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
			<a href="#" class="cta-link mt-10">Directions &amp; Parking <?php echo ens_icon('cta-link-arrow', 10); ?></a>
		</div>
	</div>
</div>
