<div class="md:text-center text-15">
	<div class="container">
		<div class="row justify-center -mt-50 -mx-40">
			<?php for ($i = 0; $i < 6; $i++) : ?>
				<div class="col sm:w-1/2 lg:w-1/3 mt-50 px-40" data-aos="fade-up" data-aos-delay="<?php echo (($i % 3) * 100) + 100; ?>">
					<h2 class="h-24">3-up Column</h2>
					<p class="mt-10">Sed posuere consectetur est at lobortis. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo.</p>

					<div data-aos="fade-up" data-aos-delay="<?php echo (($i % 3) * 100) + 200; ?>">
						<a href="#" class="cta-link text-blue mt-10">Optional CTA link <?php echo ens_icon('cta-link-arrow', 10); ?></a>
					</div>
				</div>
			<?php endfor; ?>
		</div>
	</div>
</div>
