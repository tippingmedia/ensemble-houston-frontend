<div class="section bg-blue-gradient text-white md:rounded-15">
	<div class="container">
		<div class="md:w-5/6 mx-auto">
			<blockquote class="h-36 leading-150p">The Mission of The Ensemble Theatre is to preserve African American artistic expression and enlighten, entertain and enrich a diverse community.</blockquote>
			<cite class="block font-medium roman uppercase tracking-2 opacity-60 mt-20" data-aos="fade-up">The Late George Hawkins, 1976</cite>
		</div>
	</div>
</div>
