<div class="card">
	<div class="card-media">
		<img src="https://placehold.it/720" class="bg-img" alt="">
	</div>

	<div class="card-body p-15">
		<h3 class="text-15 font-normal">Eileen J. Morris</h3>
		<p class="mt-5 text-13 text-gray"><em>Artistic Director</em></p>
	</div>
</div>
