<div class="section bg-gray-dark md:rounded-15 text-white md:text-center">
	<img src="temp/cast.jpg" class="bg-img" style="opacity: .25" alt="">

	<div class="container relative">
		<h2 class="h-24-upper">Proudly Supported By</h2>

		<ul class="list-reset text-left text-13 mt-50 text-white-80 sm:column-count-2 md:column-count-3 column-gap-40">
			<?php for ($i = 0; $i < 3; $i++) : ?>
				<li><a href="#" class="hover:text-white">Houston Arts Alliance/City of Houston</a></li>
				<li><a href="#" class="hover:text-white">Houston Endowment Inc.</a></li>
				<li><a href="#" class="hover:text-white">H-E-B</a></li>
				<li><a href="#" class="hover:text-white">Chevron</a></li>
				<li><a href="#" class="hover:text-white">Shell Oil Company</a></li>
				<li><a href="#" class="hover:text-white">Target</a></li>
				<li><a href="#" class="hover:text-white">Texas Commission on the Arts</a></li>
				<li><a href="#" class="hover:text-white">Dina Alsowayel  & Anthony Chase</a></li>
				<li><a href="#" class="hover:text-white">Kathy & Paul Anderson</a></li>
				<li><a href="#" class="hover:text-white">The Brown Foundation, Inc.</a></li>
				<li><a href="#" class="hover:text-white">The Cullen Trust for the Performing Arts</a></li>
				<li><a href="#" class="hover:text-white">The Elkins Foundation</a></li>
				<li><a href="#" class="hover:text-white">Enbridge</a></li>
				<li><a href="#" class="hover:text-white">Houston First Corporation</a></li>
				<li><a href="#" class="hover:text-white">The Shubert Foundation, Inc.</a></li>
				<li><a href="#" class="hover:text-white">United Airlines</a></li>
				<li><a href="#" class="hover:text-white">Access Data Supply, Inc. / A. Renee' Logans</a></li>
				<li><a href="#" class="hover:text-white">BP America, Inc.</a></li>
				<li><a href="#" class="hover:text-white">CenterPoint Energy</a></li>
				<li><a href="#" class="hover:text-white">Circle of Friends</a></li>
				<li><a href="#" class="hover:text-white">Linnet F. Deily</a></li>
				<li><a href="#" class="hover:text-white">Energy Software Consultants</a></li>
				<li><a href="#" class="hover:text-white">Friends of The Ensemble</a></li>
				<li><a href="#" class="hover:text-white">The Humphreys Foundation</a></li>
				<li><a href="#" class="hover:text-white">National Endowment for the Arts</a></li>
				<li><a href="#" class="hover:text-white">Brenda Peters-Chase & John Chase, Jr.</a></li>
				<li><a href="#" class="hover:text-white">Jackie Phillips</a></li>
				<li><a href="#" class="hover:text-white">Sermoonjoy Entertainment Inc.</a></li>
				<li><a href="#" class="hover:text-white">Sharon E. Murphy</a></li>
				<li><a href="#" class="hover:text-white">The Wortham Foundation, Inc.</a></li>
			<?php endfor; ?>
		</ul>

		<div class="mt-50">
			<div class="btn-group md:justify-center">
				<div data-aos="fade-up">
					<a href="#" class="btn btn-white"><span class="btn-inner">This is a Button</span></a>
				</div>
			</div>
		</div>
	</div>
</div>
