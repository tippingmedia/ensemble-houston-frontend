<div class="section bg-gray-lightest md:rounded-15 md:text-center">
	<div class="container">
		<h2 class="h-24-upper mb-50">3-up Feature Title</h2>

		<div class="row justify-center -mt-50">
			<?php for ($i = 0; $i < 3; $i++) : ?>
				<div class="col sm:w-1/2 lg:w-1/3 mt-50" data-aos="fade-up" data-aos-delay="<?php echo (($i % 3) * 100) + 100; ?>">
					<?php echo ens_partial('card'); ?>
				</div>
			<?php endfor; ?>
		</div>

		<div class="mt-50">
			<div class="btn-group md:justify-center">
				<div data-aos="fade-up">
					<a href="#" class="btn btn-blue"><span class="btn-inner">This is a Button</span></a>
				</div>
			</div>
		</div>
	</div>
</div>
