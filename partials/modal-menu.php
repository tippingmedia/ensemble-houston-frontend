<div id="modal-menu" class="modal">
	<?php echo ens_partial('search-form'); ?>

	<a href="#" class="sm:hidden btn btn-blue mt-20"><span class="btn-inner"><?php echo ens_icon('ticket', 24, 'btn-icon'); ?>Get Tickets</span></a>

	<ul class="menu-modal xl:hidden mt-20 md:mt-40">
		<li class="menu-modal-item"><a href="#" class="menu-modal-link">Our Story</a></li>
		<li class="menu-modal-item"><a href="#" class="menu-modal-link">Visit</a></li>

		<li class="menu-modal-item">
			<a href="#" class="menu-modal-link">Events</a>
			<button class="menu-modal-toggle"><?php echo ens_srt('Search') . ens_icon('navigation-show-more'); ?></button>

			<ul class="menu-modal-submenu">
				<li class="menu-modal-subitem"><a href="#" class="menu-modal-sublink">Our Story</a></li>
				<li class="menu-modal-subitem"><a href="#" class="menu-modal-sublink">Visit</a></li>
				<li class="menu-modal-subitem"><a href="#" class="menu-modal-sublink">Initiatives</a></li>
				<li class="menu-modal-subitem"><a href="#" class="menu-modal-sublink">Support Us</a></li>
			</ul>
		</li>

		<li class="menu-modal-item"><a href="#" class="menu-modal-link">Initiatives</a></li>
		<li class="menu-modal-item"><a href="#" class="menu-modal-link">Support Us</a></li>
	</ul>

	<?php echo ens_partial('social', ['classes' => 'justify-center mt-20 md:mt-40']); ?>
</div>
