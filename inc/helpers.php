<?php

function ens_script_name() {
	$parts = explode('/', $_SERVER['SCRIPT_NAME']);
	return end($parts);
}

define('ENS_SCRIPT_NAME', ens_script_name());

function ens_icon($icon = '', $size = 24, $classes = '') {
	ob_start();
	echo "<span class='icon icon-{$size} {$classes}'>";
	echo file_get_contents("assets/icons/{$icon}.svg");
	echo '</span>';
	return ob_get_clean();
}

function ens_srt($text = '') {
	return "<span class='srt'>{$text}</span>";
}

function ens_asset($path) {
	$path = 'assets/' . $path;
	return $path . '?t=' . filemtime($path);
}

function ens_include_with_vars($file, $vars = []) {
	extract($vars);
	ob_start();
	include $file . '.php';
	return ob_get_clean();
}

function ens_partial($file, $vars = []) {
	return ens_include_with_vars('partials/' . $file, $vars);
}

function ens_minify($html) {
	$html = explode("\n", $html);
	$html = array_map('trim', $html);
	$html = join('', $html);

	$html = explode("\t", $html);
	$html = array_map('trim', $html);
	$html = join('', $html);

	return $html;
}
