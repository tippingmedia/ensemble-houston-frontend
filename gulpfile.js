var gulp = require('gulp');
var include = require('gulp-include');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var rename = require('gulp-rename');

function js() {
	return gulp.src('assets/js/*.js')
		.pipe(plumber())
		.pipe(include())
		.pipe(gulp.dest('assets/dist'))
		.pipe(require('gulp-uglify')())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest('assets/dist'));
}

function css() {
	var sass = require('gulp-sass');

	return gulp.src('assets/scss/*.scss')
		.pipe(plumber())
		.pipe(include())
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss([
			require('tailwindcss')('tailwind.js'),
			require('postcss-easing-gradients'),
			require('autoprefixer'),
		]))
		.pipe(gulp.dest('assets/dist'))
		.pipe(postcss([
			require('postcss-clean'),
		]))
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest('assets/dist'));
}

function watch() {
	gulp.watch(['assets/scss/**/*', 'tailwind.js'], gulp.series(css));
	gulp.watch('assets/js/**/*', js);
}

exports.css = css;
exports.js = js;
exports.default = watch;
